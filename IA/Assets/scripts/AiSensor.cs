using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class AiSensor : MonoBehaviour
{
    public float VisionAngle = 45;
    public Color VisionColor;
    public float MaxVisionDistance = 20;
    public LayerMask mask;

    [Serializable]

    public class OnPlayerDtectClass : UnityEvent { }
    [FormerlySerializedAs("OnDectPlayer")]
    [SerializeField]
    public OnPlayerDtectClass m_OndectectPlayer = new OnPlayerDtectClass();


    [Serializable]
    public class OnPlayerLost : UnityEvent { }
    [FormerlySerializedAs("OnDectPlayer")]
    [SerializeField]
    public OnPlayerLost m_OnLostPlayer = new OnPlayerLost();
    private void Update()
    {
        Vector3 targetdirection = AIManager.instance.Player.position - transform.position;
        float Angle = Vector3.Angle(targetdirection, transform.forward);
        if (Angle < VisionAngle)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, targetdirection, out hit, MaxVisionDistance, mask))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.transform == AIManager.instance.Player)
                    {
                        Debug.DrawRay(transform.position, targetdirection, Color.red);
                        m_OndectectPlayer?.Invoke();
                    }
                    else
                    {
                        m_OnLostPlayer?.Invoke();
                    }
                }


            }
        }
        else
        {
            m_OnLostPlayer?.Invoke();
        }
    }
}

#if UNITY_EDITOR
[ExecuteAlways]
[CustomEditor(typeof(AiSensor))]
public class EnemyVsionSensor : Editor
{
    public void OnSceneGUI()
    {
        var ai = target as AiSensor;
        Vector3 starPoint = Mathf.Cos(-ai.VisionAngle * Mathf.Deg2Rad) * ai.transform.forward + Mathf.Sin(ai.VisionAngle * Mathf.Deg2Rad) * -ai.transform.right;
        Handles.color = ai.VisionColor;
        Handles.DrawSolidArc(ai.transform.position, Vector3.up, starPoint, ai.VisionAngle * 2f, ai.MaxVisionDistance);
    }
}
#endif